<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Article;
use App\Entity\Category;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i<4 ; $i++){
            $category = new Category();
            $category   ->setTitle('Cat n°'.$i)
                        ->setDescription('Je suis une description lol');
            $manager->persist($category);
        }

        for ($i=0; $i<4; $i++){
            $article = new Article();
            $article->setTitle("Article n°".$i)
                    ->setDescription("Je suis une description description")
                    ->setContent("Le lorem ipsum ne marche pas ALED ;o;")
                    ->setCreatedAt(new \DateTime());
            $manager->persist($article);
        }


        $manager->flush();
    }
}
