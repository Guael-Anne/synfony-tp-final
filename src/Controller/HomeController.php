<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ArticleType;
use App\Form\CatType;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(): Response
    {
        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/articles", name="articles")
     */
    public function articles(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $articles = $repo->findAll();

        return $this->render('home/articles.html.twig', ["articles" => $articles]);
    }

    /**
     * @Route("/article/{id}", name="article_detail")
     */
    public function article(Article $article): Response
    {
        return $this->render('home/article_detail.html.twig', [
            "article" => $article
            ]);
    }

    /**
     * @Route("/ajouter-article", name="new_article")
     * @Route("/edit-article/{id}", name="article_edit")
     */
    public function new_edit_article(Article $article = null, Request $request, ObjectManager $manager): Response
    {
        if (!$article) {
            $article = new Article();
        }
        
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$article->getId()) {
                $article->setCreatedAt(new \DateTime());
            }
    
            $manager->persist($article);
            $manager->flush();
    
            return $this->redirectToRoute('article_detail', [
                "id" => $article->getId()
            ]);
        }

        return $this->render('home/new_article.html.twig', [
            'form' => $form->createView(),
            'editMode' => $article->getId() !== null
        ]);
    }   
    
    /**
     * @Route("/nouvelle-categorie", name="new_cat")
     * @Route("/edit-category/{id}", name="cat_edit")
     */
    public function new_cat(Category $category = null, Request $request, ObjectManager $manager): Response
    {
        if (!$category) {
            $category = new Category();
        }
        
        $form = $this->createForm(CatType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
    
            $manager->persist($category);
            $manager->flush();
    
            return $this->redirectToRoute('cat_detail', [
                "id" => $category->getId()
            ]);
        }

        return $this->render('home/new_cat.html.twig', [
            'form' => $form->createView(),
            'editMode' => $category->getId() !== null
        ]);
    }

    /**
     * @Route("/category/{id}", name="cat_detail")
     */
    public function detailcat(Category $category): Response
    {
        return $this->render('home/cat_detail.html.twig', [
            "category" => $category
            ]);
    }

}
